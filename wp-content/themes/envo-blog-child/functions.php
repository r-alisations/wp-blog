<?php 
function theme_enqueue_styles() 
{
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function envo_blog_generate_construct_footer() 
{
?>
<p class="footer-credits-text text-center">
	<?php 
	echo '© COPYRIGHT 2019 - JEAN FORTEROCHE - TOUS DROITS RÉSERVÉS';
	?>
</p> 
<?php
}
add_action( 'envo_blog_generate_footer', 'envo_blog_generate_construct_footer' );